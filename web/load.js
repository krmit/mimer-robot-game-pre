"use strict";
import Goal from "./things/goal.js";
import Resourse from "./things/resourse.js";
import Source from "./things/source.js";

export default function load(levelData, map, resourses) {
  for (let resurse of levelData.resourses) {
    let result;
    if (resurse.source) {
      let mushroom_data_list = [];
        for(let name of resurse.source) {
          let mushroom_data = resoursesImages[name];
          mushroom_data_list.push(mushroom_data);
        }
        result = new Source(
          resurse.x,
          resurse.y,
          resurse.source[0]+"#"+resurse.source.length,
          mushroom_data_list
        );
        map[resurse.x][resurse.y].source = result; 
        //map[resurse.x][resurse.y].get();
    } else {
      let mushroom_data = resoursesImages[resurse["resourse"]];
      result = new Resourse(
        resurse.x,
        resurse.y,
        resurse.name ? resurse["resourse"] : resurse.name,
        mushroom_data[0],
        mushroom_data[1],
      );
      map[resurse.x][resurse.y].resourse = result;
    }
    resourses.push(result);
  }

  if (levelData.goals !== undefined) {
    for (let resurse of levelData.goals.resourses) {
      let mushroom_data = resoursesImages[resurse["resourse"]];
      map[resurse.x][resurse.y].goal = new Goal(
        resurse.x,
        resurse.y,
        resurse.name ? resurse.name : resurse["resourse"],
        mushroom_data[0],
        mushroom_data[1],
      );
    }
  }
}

const resoursesImages = {
  "mushroom-red-dot": ["mushrooms", 1],
  "mushroom-yellow-dot": ["mushrooms", 3],
  "mushroom-green-dot": ["mushrooms", 5],
  "mushroom-blue-dot": ["mushrooms", 6],
  "mushroom-red-star": ["mushrooms", 8],
  "mushroom-yellow-star": ["mushrooms", 10],
  "mushroom-green-star": ["mushrooms", 11],
  "mushroom-blue-star": ["mushrooms", 12],
  "mushroom-red-square": ["mushrooms", 14],
  "mushroom-yellow-square": ["mushrooms", 16],
  "mushroom-green-square": ["mushrooms", 18],
  "mushroom-blue-square": ["mushrooms", 19],
};

// function for debug map.
export function logMap(map) {
  let result = "";
  for(let i = 0 ; i < map.length; i++) {
    for(let j = 0 ; j < map[i].length; j++) {
      switch (map[i][j]) {
        case undefined:
        result += "u";
        break;
        case 0:
        result += " ";
        break;
        case 8:
        result += "─";
        break;
        case 10:
        result += "─";
        break;
        case 11:
        result += "│";
        break;
        case 9:
        result += "│";
        break;
        case 4:
        result += "┌";
        break;
        case 5:
        result += "┐";
        break;
        case 6:
        result += "┘";
        break;
        case 7:
        result += "└";
        break;
        default:
          if(map[i][j].name) {
             result += map[i][j].name[0];
          } else {
            console.log(map[i][j]);
            result += "E";
          }
         break;
      }
    }
    result += "\n"
  }

  console.log(result);
}