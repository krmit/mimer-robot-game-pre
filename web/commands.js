CodeMirror.commands.autocomplete = function (cm) {
  cm.showHint({ hint: CodeMirror.hint.anyword });
};

CodeMirror.commands.save = function () {
  //console.log("Saving data with id " + filePath);

  const position = editor.doc.getCursor();

  let nice_code = prettier.format(editor.getValue(), {
    parser: "babylon",
    plugins: prettierPlugins,
  });

  editor.setValue(nice_code);
  editor.doc.setCursor(position);
  /*fetch(filePath, {
    method: "POST",
    body: nice_code
  });*/
};

const dir_js_word = [
  "function (){\n\n}",
  "const",
  "let",
  "for(let i = 0;i < .length;i++){\n\n}",
  'console.log("");',
  "if(===){\n\n}",
  '= require("");',
  'require("");',
];

const orig = CodeMirror.hint.anyword;
CodeMirror.hint.anyword = function (cm) {
  let word = "";

  let cursor = editor.getCursor();
  let end = cursor.ch;
  let start = end;

  let line = editor.getLine(cursor.line);
  while (true) {
    let ch = line.charAt(end);
    if (" \t\n\r\v".indexOf(ch) > -1 && start != 0) {
      start--;
    } else {
      word = line.slice(start, end);
      break;
    }
  }

  let result = orig(cm);
  if (result === undefined) {
    result = { from: start, to: end, list: [] };
  }

  let filter_words = dir_js_word.filter((element) => element.startsWith(word));
  result.list = filter_words.concat(
    result.list.filter(function (item) {
      return filter_words.indexOf(item) < 0;
    }),
  );
  return result;
};
