"use strict";
import Resource from "./resourse.js";
import {logMap} from "../load.js";

export default class Sourse extends Resource {
  constructor(x, y, name, frames, random = false) {
    super(x, y, name + "#0", frames[0][0], frames[0][1]);
    this.frames = frames;
    this.index = 0;
    this.random = random;
  }

  get() {
    super.get();
    this.index++;
      if (this.frames.length > this.index) {
        let data = this.frames[this.index];
        this.map[this.x][this.y].resource = new Resource(
          this.x,
          this.y,
          this.name + " #" + this.index,
          data[0],
          data[1],
        );
        console.log(this.map[this.x][this.y]);
      }
  }
}
