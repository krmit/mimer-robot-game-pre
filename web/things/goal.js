"use strict";
import Things from "./things.js";

let goal_resourses = [];

export default class Goal extends Things {
  constructor(x, y, name, image, frame) {
    super(x, y, name, image, frame);
    this.done = false;

    this.image.setAlpha(0.5);
    goal_resourses.push(this);
  }

  equivalent(target_goal) {
    if (this.resourse === target_goal.resourse) {
      this.done = true;
      let all_done = true;
      for (let goal of goal_resourses) {
        if (!goal.done) {
          all_done = false;
          break;
        }
      }

      if (all_done) {
        console.log("finish");
        Things.emitter.emit("finish");
      }
      return true;
    } else {
      return false;
    }
  }
}
