"use strict";
import Things from "./things.js";
import {logMap} from "../load.js";

export default class Robot extends Things {
  constructor(x, y, name, image, frame) {
    super(x, y, name, image, frame);
    this.cargo = null;
    this.stepTime = 1000;
  }

  async run(code) {
    code = code.replace(/up\(\)/g, "await this.Up()");
    code = code.replace(/down\(\)/g, "await this.Down()");
    code = code.replace(/left\(\)/g, "await this.Left()");
    code = code.replace(/right\(\)/g, "await this.Right()");
    code = code.replace(/put\(\)/g, "await this.Put()");
    code = code.replace(/pick\(\)/g, "await this.Pick()");
    code = code.replace(/log\(/g, "await this.Log(");

    eval("this.f = async function() {" + code + "};this.f();");
  }

  Up() {
    return this.move(0, -1);
  }

  Down() {
    return this.move(0, 1);
  }

  Left() {
    return this.move(-1, 0);
  }

  Right() {
    return this.move(1, 0);
  }

  Pick() {
    return new Promise((resolve) => {
      this.cargo = this.map[this.x][this.y].resource;
      this.cargo.get();
      resolve(this.cargo.name);
    });
  }

  Put() {
    return new Promise((resolve) => {
      if (this.map[this.x][this.y].goal.done === false) {
        this.map[this.x][this.y].goal.equivalent(this.cargo);
      }
      this.map[this.x][this.y].resourse = this.cargo;
      this.cargo = null;
      let result = "done";
      resolve(result);
    });
  }

  Log(msg) {
    console.log(msg);
    Things.console += msg + "\n";
    document.getElementById("console").value = Things.console;
  }

  move(x, y) {
    let _this = this;
    return new Promise((resolve) => {
      _this.x += x;
      _this.y += y;
      let targets = [_this.image];
      if (_this.cargo !== null) {
        targets.push(_this.cargo.image);
      }

      _this.scene.tweens.add({
        targets: targets,
        x: _this.image.x + x * 128,
        y: _this.image.y + y * 128,
        duration: this.stepTime,
        ease: "Linear",
        onComplete: () => resolve(),
      });
    });
  }
}