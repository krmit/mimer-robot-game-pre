"use strict";

export default class Things {
  static setupThings(scene, width, heigth, emitter, text) {
    Things.scene = scene;

    // A map of numbers representing frames, should not be here.
    Things.map = createGameMap(width,heigth);
    Things.height = heigth;
    Things.width = width;
    Things.emitter = emitter;
    Things.console = text;
    Things.levelText = text;
  }

  constructor(x, y, name, image, frame = 0) {
    this.x = x;
    this.y = y;
    this.ox = x;
    this.oy = y;
    this.name = name;
    this.image = Things.scene.add.sprite(
      x * 128 + 63,
      y * 128 + 63,
      image,
      frame,
    );
    this.scene = Things.scene;
    this.map = Things.map;
    console.log(Things.map);
  }

  get() {
    this.map[this.x][this.y].resourse = null;
  }

  set(x, y) {
    console.log(this.map);

    this.map[x][y].resource = this;
    this.x = x;
    this.y = y;
    this.image.x = x * 128 + 64;
    this.image.y = y * 128 + 64;
  }

  reset() {
    this.set(this.ox, this.oy);
  }
}

function createGameMap(width, heigth) {
  const result = [];

  for (let i = 0; i < width; i++) {
    result[i] = []
    for (let j = 0; j < heigth; j++) {
    result[i][j] = {};
    }
  }

  return result;
}
