"use strict";

import Things from "./things/things.js";
import load from "./load.js";
import Robot from "./things/robot.js";
import {logMap} from "./load.js";

let robot;
let console_text;
let stepTime;
let resourses = [];

const emitter = new Phaser.Events.EventEmitter();
let level_data_description;
let editor;
let prompt_data = [];
let prompt_index = 0;

const _prompt = prompt;

prompt = function (msg) {
  let result;
  if (!prompt_data) {
    result = _prompt(msg);
  } else {
    result = prompt_data.values[prompt_index];
    prompt_index++;
  }

  return result;
};

export function startGame() {
  //Frome https://stackoverflow.com/questions/10609511/javascript-url-parameter-parsing
  const GET = (function () {
    const get = {
        push: function (key, value) {
          var cur = this[key];
          if (cur.isArray) {
            this[key].push(value);
          } else {
            this[key] = [];
            this[key].push(cur);
            this[key].push(value);
          }
        },
      },
      search = document.location.search,
      decode = function (s, boo) {
        var a = decodeURIComponent(s.split("+").join(" "));
        return boo ? a.replace(/\s+/g, "") : a;
      };
    search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function (a, b, c) {
      if (get[decode(b, true)]) {
        get.push(decode(b, true), decode(c));
      } else {
        get[decode(b, true)] = decode(c);
      }
    });
    return get;
  })();

  if (GET.steptime === undefined) {
    stepTime = 1000;
  } else {
    stepTime = GET.steptime * 1000;
  }

  editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
    lineNumbers: true,
    mode: "javascript",
    matchBrackets: true,
    gutters: ["CodeMirror-lint-markers"],
    lint: { options: { esversion: 6, node: true } },
    theme: "dracula",
    keyMap: "mimer",
  });

  class RobotGameField extends Phaser.Scene {
    constructor(config) {
      super(config);
      Phaser.Scene.call(this, { key: "RobotGameField", active: true });
      this.gameOn = true;
    }

    preload() {
      this.load.image(
        "simple-titles",
        "../assets/titles/simple-titles-128x128-4x3.png",
      );
      this.load.image("robot-red", "../assets/robots/robot-red-128x128.png");
      this.load.image("robot-blue", "../assets/robots/robot-blue-128x128.png");
      this.load.spritesheet(
        "mushrooms",
        "../assets/icons/mushrooms-diffrent-color-128x128-7x3.png",
        { frameWidth: 128, frameHeight: 128 },
      );
      this.load.bitmapFont(
        "ice",
        "../assets/bitmapfonts/iceicebaby.png",
        "../assets/bitmapfonts/iceicebaby.xml",
      );

      if (GET["level"] !== undefined) {
        this.load.json("basic-game", "../rooms/" + GET["level"] + ".json");
      } else {
        this.load.json("basic-game", "../rooms/basic.json");
      }
    }

    create() {
      // Load a map from a 2D array of tile indices
      const level_data = game.cache.json.get("basic-game");

      document.getElementById("editor-headline").innerHTML = level_data.name;
      editor.setValue(level_data.code);
      level_data_description = level_data.description + "\n";
      console_text = level_data_description;
      document.getElementById("console").value = console_text;

      prompt_data = level_data.prompt;

      // When loading from an array, make sure to specify the tileWidth and tileHeight
      let map = this.createGameBoard(level_data);
      Things.setupThings(this, level_data.width,level_data.heigth, emitter, console_text);

      var titlemap = this.make.tilemap({
        data: map,
        tileWidth: 128,
        tileHeight: 128,
      });
      var tiles = titlemap.addTilesetImage("simple-titles");
      var layer = titlemap.createStaticLayer(0, tiles, 0, 0);

      load(level_data, Things.map, resourses);
      const win_citation = level_data["win-citation"];
      const level_name = level_data.name;
      const level_next = level_data.next;

      emitter.on(
        "finish",
        () => {
          const text = _this.add
            .bitmapText(400, 300, "ice", win_citation, 96)
            .setOrigin(0.5);

          _this.tweens.add({
            targets: text,
            duration: 2000,
            angle: 360,
            ease: "Quad.easeInOut",
            repeat: -1,
            yoyo: true,
          });

          setTimeout(function () {
            alert(
              "Du har klarat rummet: " +
                level_name +
                "\nDu kan nu förska klara nästa rum: " +
                level_next,
            );
            window.location.href = "../index.html";
          }, 5000);
        },
        this,
      );

      let _this = this;
      let camera_x = null;
      let camera_y = null;

      this.input.on("pointermove", function (pointer) {
        if (_this.ctrl.isDown) {
          if (!(camera_x === null && camera_y === null)) {
            let x_diff = pointer.x - camera_x;
            let y_diff = pointer.y - camera_y;
            _this.cameras.main.scrollX -= x_diff;
            _this.cameras.main.scrollY -= y_diff;
          }
          camera_x = pointer.x;
          camera_y = pointer.y;
        } else {
          camera_x = null;
          camera_y = null;
        }
      });

      this.ctrl = this.input.keyboard.addKey(
        Phaser.Input.Keyboard.KeyCodes.CTRL,
      );
      this.cameras.main.setBounds(0, 0, layer.width, layer.height);

      const robot_data = level_data["robots"][0];
      robot = new Robot(
        robot_data.x,
        robot_data.y,
        "Robot",
        robot_data.robot,
        0,
      );

      document.getElementById("robot-headline").innerHTML = robot_data.name;
    }

    update(time, delta) {}

    // _Number is the index of a frame for the background.
    createGameBoard(level) {
      const result = [];

      result[0] = new Array(level.width).fill(8);
      result[0][0] = 4;
      result[0][level.width - 1] = 5;
      for (let i = 1; i < level.heigth - 1; i++) {
        result[i] = new Array(level.width).fill(0);
        result[i][0] = 11;
        result[i][level.width - 1] = 9;
      }
      result[level.heigth - 1] = new Array(level.width).fill(10);
      result[level.heigth - 1][0] = 7;
      result[level.heigth - 1][level.width - 1] = 6;

      return result;
    }
  }

  const config = {
    type: Phaser.WEBGL,
    width: 800,
    height: 800,
    backgroundColor: "#000",
    parent: "robot-field",
    scene: [RobotGameField],
  };

  const game = new Phaser.Game(config);
}

export function resetRoom() {
  prompt_index = 0;
  robot.reset();
  for (let resourse of resourses) {
    resourse.reset();
  }
  Things.console = Things.levelText;
  document.getElementById("console").value = Things.console;
}

export function runCode() {
  resetRoom();
  console_text = level_data_description + "\n";
  document.getElementById("console").value = console_text;
  robot.run(editor.getValue());
}
