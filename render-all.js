#!/usr/bin/node
"use strict";

const fs = require("fs-extra");
require("colors");
const glob = require("glob");
const yaml = require("js-yaml");

let room_path;
try {
  const room_file_paths = glob.sync(__dirname + "/rooms/*.yml");
  for (room_path of room_file_paths) {
    const doc = yaml.safeLoad(fs.readFileSync(room_path, "utf8"));
    fs.writeFileSync(room_path.split(".")[0] + ".json", JSON.stringify(doc));
  }
} catch (e) {
  console.log(room_path);
  console.log(e);
}
