# Todo

## Gör det "spelbart"

- ladda upp det på hemsidan.
- !prompt
- !log
- !Skapa demo med att plocka upp resurs
- !load
- !put
- !Gör så att alla rum kan väljas med hjälp av REST.
- !Gör så att vi har en index fil i root.
- !Gör så vi restera robotens rörelsen.
- !Fixa så vi kan styra tiden med RestFull
- !Få kamran att fungera utan zoom.
- !Gör så vi resterar även resurser.

## Utveckla rummen

- !Fundera hur målvilkoren ska skrivas in i yml.
- !Gör så att roboten väljas av rummet och var roboten ska starta.
- !Skriv ut rummets namn istället för editor.
- !Under det skriv robotens namn.
- !Låt rummet bestämma vilken kod som ska visas i editorn.
- !Skriv ut beskrivningen i konsol.
- !Gör så att mål positionen för resurserna syns, med effekter.
- !Gör så att rummet klaras av när alla mål är klara.
- !Gör en vinnar animation.
- !Rekommendera att göra nästa rum.
- !Skriv några standar rum och testa att de fungerar.

## Förbättrakoden

- !Dela upp koden så det är inte är mycket js i html koden.
- !Ta bort alla filer som inte behövs.
- !Fixa till så koden den ser bra ut med prettier.
- !Gå igenom och rätta kodnemanuellt, fixa namn etc.

## Testa att det går med flera robotar

- Gör så man kan välja en robot på spelplan.
- Gör så att man kan välja en robot och visa iconen.
- låt koden ändras beronde på vilken robot du har valt
- Vid "kör" sa alla robotor köras samtidigt.
- Låt rummen bestämma var robotarna ska vara.

## Kod

- Gör så editorn i spelet använder sig av prettier.
- Gör så kod sparas för varje robbot och varje rum som cookises.
- Lägg till flera gools beronde på kodens kvalite, börja med hur många "lines"
  den innehåller.
- Hur många av ett vist nyckelord koden har, kontrollera max och min.
- Skriven en varning i konsolen om inte ett goal uppfylls.

## Language

- Gör så alla texter har versioner för olika språk
- Skriv version för en och
  gb.https://tyda.se/search/gestalt?lang%5B0%5D=en&lang%5B1%5D=sv
- Gör så att språk visas beronde på en configuration.
- Gör så att denna konfiguration går att välja enkelt.

## Kamera

- Få den att fungera smidigt för rörelse.
- Få vetiga tangenter för zoom.
- Fixa så zoom fungerar.
- Skriv ett enkelt demo på zoom bugen.
- Sök relevant information.
- Skriv in en bug rapport.

## Meny

- Gör en configurations fil för menyn, där infor om alla godkända rum och deras
  depenesis finns.
- Skapa en meny som kan visas ovan på spelet när den anropas.
- Gör den som en phaser canvas.
- I denna visa alla rum med de enklaste överst. Och sedan berond på nivå.
- Gör så att man får se information om ett rummen som visas i menyn.
- Gör så du kan välja ett rum genom att klicka på det.
- Gör så att man kan tydligt se status på rum så som avklarat, påbörjad, stängd
  och öppet.
- Gör pilar mellan rum.
- Rita ut med pilar mellan rum som har dependensis.

## Förbättra spelet

- Nu font som jag har licens för.
- Gör ett så resurser kan slumpas fram. Bra för if-satser.
- Få bort alla globala variabeler.
- Gå igenom min css fil och se om jag kan förbättra
- Gör så vi får eller kan få olika css filer.

## Förbered publisering

- Se till att editorn koden importeras direkt ifrån mimer-editor
- Skriv en bra insruktion till spelet på svenska
- Översätta denna till engelska.
- Skriv en kort README
- Fixa alla bugar jag känner till, notera problem som är förstora för att fixa.
- Se till att jag har ett bra verktyg för att analysera användingen av
  programmet/sajten.
- Byt arkiv till en slutgiltigt.
